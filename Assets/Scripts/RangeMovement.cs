﻿using UnityEngine;
using System.Collections;

public class RangeMovement : MonoBehaviour {
	public float speed = 0.1f;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		if(Input.GetKey(KeyCode.W))
		{
			this.gameObject.transform.position += new Vector3(speed,0,speed);
		}
		if(Input.GetKey(KeyCode.S))
		{
			this.gameObject.transform.position += new Vector3(-speed,0,-speed);
		}
		if(Input.GetKey(KeyCode.D))
		{
			this.gameObject.transform.position += new Vector3(speed,0,-speed);
		}
		if(Input.GetKey(KeyCode.A))
		{
			this.gameObject.transform.position += new Vector3(-speed,0,speed);
		}

	
	}
}

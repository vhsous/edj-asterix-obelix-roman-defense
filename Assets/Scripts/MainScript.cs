﻿using UnityEngine;
using System.Collections;



public class MainScript : MonoBehaviour {

	public float velocity;
	public GameObject enemyFab;
	public GameObject healthLabelFab;
	public Camera mainCam;
	public GameObject obelix;
	public GameObject asterix;
	public float x;
	public float y;
	public float z;
	public float coef;

	private float obX;
	private float obZ;
	// Use this for initialization
	void Start () {
		y = 0.1f;
		coef = 0.4f;
		velocity = 0.3f;
	}

	
	
	// Update is called once per frame
	void Update () {
		spawnEnemy ();
		updateCamera ();
		
	}
	//period of soldier spawning
	float timer=2;

	void updateCamera()
	{
		if (GameObject.Find ("Range(Clone)") == null) {
			obX = obelix.transform.position [0];
			obZ = obelix.transform.position [2];
			
		} else {
			GameObject range = GameObject.Find("Range(Clone)");
			obX = range.transform.position [0];
			obZ = range.transform.position [2];
		}
		
		float asX = asterix.transform.position [0];
		float asZ = asterix.transform.position [2];
		
		float x = Mathf.Min(obX,asX) + Mathf.Abs(obX - asX)/2;
		float z = Mathf.Min(obZ,asZ) + Mathf.Abs(obZ - asZ)/2;
		
		float diff = Mathf.Sqrt (Mathf.Pow (obX - asX, 2) + Mathf.Pow (obZ - asZ, 2));

		diff *= coef;
		mainCam.orthographicSize = Mathf.Clamp (diff, 1.5f, 4);
		
		mainCam.transform.position = new Vector3 (x-3.5f,5,z-3.5f);
		//Debug.Log (x + " " + z);
		
	}

	void spawnEnemy(){
		timer -= Time.deltaTime;
		if (timer<=0){
			timer=3;
			int rand = Random.Range(1,4);
			//Debug.Log("Soldier Spawn in position "+rand);
			GameObject newEnemy=null;
			if(rand==1){
				newEnemy = (GameObject) Instantiate(enemyFab, new Vector3(-1.5f,y,-6f), transform.rotation);
				newEnemy.rigidbody.velocity = new Vector3(0,0,velocity);
				//newEnemy.GetComponent<HPScript>().moveDirection = new Vector3(0,0,velocity);
			}
			if(rand==2){
				newEnemy = (GameObject) Instantiate(enemyFab, new Vector3(6f,y,-1.5f), transform.rotation);
				newEnemy.rigidbody.velocity = new Vector3(-velocity,0,0);
				//newEnemy.GetComponent<HPScript>().moveDirection = new Vector3(-velocity,0,0);
			}
			if(rand==3){
				newEnemy = (GameObject) Instantiate(enemyFab, new Vector3(1.5f,y,6f), transform.rotation);
				newEnemy.rigidbody.velocity = new Vector3(0,0,-velocity);
				//newEnemy.GetComponent<HPScript>().moveDirection = new Vector3(0,0,-velocity);

			}
			if(rand==4){
				newEnemy = (GameObject) Instantiate(enemyFab, new Vector3(-6f,y,1.5f), transform.rotation);
				newEnemy.rigidbody.velocity = new Vector3(velocity,0,0);
				//newEnemy.GetComponent<HPScript>().moveDirection = new Vector3(velocity,0,0);
			}
			//Instatiate a health bar
			GameObject newHealthBar = (GameObject) Instantiate(healthLabelFab, new Vector3(100,0,100), transform.rotation );
			HealthLabel hp = newHealthBar.GetComponent<HealthLabel>();
			//nBars=total health/50
			hp.nbars=Mathf.RoundToInt(newEnemy.GetComponent<HPScript>().health/50f);
			hp.setPropreties();
			//HealthLabelFab must follow the new enemy
			hp.target=newEnemy.transform;
			newEnemy.GetComponent<HPScript>().healthBar=hp;
		}
	}
}

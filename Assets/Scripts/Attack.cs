﻿using UnityEngine;
using System.Collections;

public class Attack : MonoBehaviour {


	public float damage;
	public float damage_potion;
	public KeyCode k;
	bool ableToAttack = false;
	float attack_timer;
	float direction;
	float distanceTo;
	Collider[] cls;
	public float cooldown;

	// Use this for initialization
	void Start () {
		attack_timer = 0;
		cooldown = 2.0f;
	}
	
	// Update is called once per frame
	void Update () {
	
		if (attack_timer > 0) {
			attack_timer-=Time.deltaTime;

		}else{
			attack_timer = 0;
		}


		if(Input.GetKeyDown (k))
			attack();




	}

	void attack()
	{
		
		cls = Physics.OverlapSphere(transform.position, 100, 1);
		
		for(int i = 0; i<=cls.Length-1; i++)
			
		{
			
			//print(cls[i].gameObject.transform.name);
			
			if(cls[i].name.ToLower().Contains("soldier"))
			{
				distanceTo = Vector3.Distance(cls[i].gameObject.transform.position, this.transform.position);

				print (distanceTo);
				if(distanceTo<2.5f)
				{
					Vector3 dir = (cls[i].transform.position - this.transform.position).normalized;
					
					direction = Vector3.Dot(dir,this.transform.forward);
					
					if(attack_timer ==0 && (direction>0))
					{
						cls[i].SendMessage("ApplyDamage",damage);
						//TODO: if potion
						
						attack_timer = cooldown;
					}
				}
			}
			
			
			
		}
	}
	void OnGUI()
	{
		GUILayout.Label ("Attack!");
	}

	void OnTriggerEnter(Collider col)
	{


		if (col.gameObject.name.ToLower ().Contains ("soldier")) 
		{


		
			//if (Input.GetKeyDown (k)) {

			Debug.Log(attack_timer +" "+ direction);



			//}
			//Debug.Log("Direction: "+direction);
		}
		
	}
}

﻿using UnityEngine;
using System.Collections;

public class HPScript : MonoBehaviour {

	public float health;
	public HealthLabel healthBar;
	// Use this for initialization

	//public Vector3 moveDirection;

	void Start () {
		health = 150;
	}
	
	// Update is called once per frame
	void Update () 
	{

	}

	void FixedUpdate()
	{
		//this.transform.position += moveDirection*Time.deltaTime;
	}

	void ApplyDamage(int damage){
		health -= damage;
		Debug.Log ("Soldado levou com" + damage);
		if (health <= 0) 
		{
			//animation.play(die)
			Destroy(healthBar.gameObject);
			Destroy(this.gameObject);
		}
		else
			healthBar.ReduceLife (damage/50);
	}


	void OnTriggerEnter(Collider hit)
	{
		if (hit.gameObject.name == "flag") {
			Debug.Log (hit.gameObject.name);
			Destroy(healthBar.gameObject);
			Destroy(this.gameObject);
		}
	}
}

﻿using UnityEngine;
using System.Collections;

public class Throw : MonoBehaviour
{
	public KeyCode key = KeyCode.E;
	public KeyCode fire = KeyCode.F;
	public GameObject range;
	public float ThrowForce = 10f; //How strong the throw is. This assumes the picked object has a rigidbody component attached
	public float AlphaAmount = 0.5f; //this will be the alpha amount. It's public so you can change it in the editor

	private MonoBehaviour objectInput; 
	private GameObject objectRange;
	private Transform _pickedObject;
	private Color _originalColor;
	private bool collidingWithMenhir = false;
	private bool engaged = false;
	private float keyCooldown = 1.0f;
	private float keyTime = 0;
	private bool ableToFire = false;
	private GameObject menir;
	void OnTriggerEnter (Collider collision)
	{
		if ( collision.transform.tag.ToLower().Contains("pickup" ))
		{


			collidingWithMenhir = true;

			menir = collision.gameObject;
			/*//caches the picked object
			_pickedObject = collision.transform;
			
			//caches the picked object's color for resetting later
			_originalColor = _pickedObject.renderer.material.color;
			
			//this will snap the picked object to the "hands" of the player
			_pickedObject.position = HoldPosition.position;
			
			//this will set the HoldPosition as the parent of the pickup so it will stay there
			_pickedObject.parent = HoldPosition;
			
			//this will change the alpha amount on the object's color to make it half transparent
			_pickedObject.renderer.material.color = new Color(_pickedObject.renderer.material.color.r,
			                                                  _pickedObject.renderer.material.color.g,
			                                                  _pickedObject.renderer.material.color.b,
			                                                  AlphaAmount);*/
			
		}
	}

	void OnTriggerExit(Collider col)
	{

		if (col.transform.tag.ToLower ().Contains ("pickup")) {
						
			//print ("Stopped colliding!");

			collidingWithMenhir = false;
			menir = null;
		}
	}	
	void Start()
	{
		objectInput = this.gameObject.GetComponent("PlatformInputController") as MonoBehaviour;

	}
	void Update()
	{
		keyTime -= Time.deltaTime;
		if (collidingWithMenhir) {

			print ("Is the key pressed?: "+Input.GetKey(key) +" "+ key);
			if (Input.GetKey (key) && keyTime <=0) {
				keyTime = keyCooldown;

				if(!engaged)
				{
					print ("Activating seek mode...");
					objectInput.enabled = false;

					objectRange = (GameObject) Instantiate(range, this.gameObject.transform.position, this.gameObject.transform.rotation);

					ableToFire = true;



				}else {
					print ("Deactivating seek mode...");
					objectInput.enabled = true;
					Destroy(objectRange);
				}

				engaged = !engaged;
			}
		}

		if (Input.GetKey (fire) && ableToFire) {
					

			ableToFire = false;

			fireMenhir();


			objectInput.enabled = true;
			Destroy(objectRange);

			//TODO: Play anim throw	
		}

	}

	void fireMenhir()
	{


	}


}
﻿using UnityEngine;
using System.Collections;

public class CharacterBehaviour : MonoBehaviour {

	ArrayList soldiersOnRange ;
	float cooldown;
	int stamina;

	public KeyCode attackKey;
	public int hitDamage;

	// Use this for initialization
	protected void Start () {
		cooldown = 0;
		soldiersOnRange = new ArrayList();
		stamina = 100;
	}
	
	
	// Update is called once per frame
	protected void Update () {
		cooldown -= Time.deltaTime;
		if (cooldown < 0)
			cooldown = 0;
		//Soldiers might have been destroyed, so they must be removed
		soldiersOnRange.Remove (null);
		if(soldiersOnRange.Count>=1 && Input.GetKey(attackKey) && cooldown==0){
			foreach(GameObject soldier in soldiersOnRange){
				soldier.SendMessage("ApplyDamage",hitDamage);
			}
			cooldown=2;
		}
	}
	
	void OnTriggerEnter(Collider col)
	{
		if (col.gameObject.name == "Obelix") {
			Debug.Log("Asterix bumped Obelix");		
		}
		if (col.gameObject.name == "Asterix") {
			Debug.Log("Obelix bumped Asterix");		
		}
		if (col.gameObject.name.ToLower().Contains("soldier")) {
			Debug.Log("Asterix/Obelix bumped Soldier");
			soldiersOnRange.Add(col.gameObject);
		}
	}
	
	void OnTriggerExit(Collider col)
	{
		if (col.gameObject.name.ToLower().Contains("soldier")) {
			soldiersOnRange.Remove(col.gameObject);
		}
		
	}


}

﻿using UnityEngine;
using System.Collections;

public class HealthLabel : MonoBehaviour {
	public GUITexture guitex;

	public Transform target;  // Object that this label should follow

	public Vector3 offset;    // Units in world space to offset; 1 unit above object by default
	public bool clampToScreen = false;  // If true, label will be visible even if object is off screen
	public float clampBorderSize = 0.05f;  // How much viewport space to leave at the borders when a label is being clamped
	public bool useMainCamera = true;   // Use the camera tagged MainCamera
	public Camera cameraToUse ;   // Only use this if useMainCamera is false
	Camera cam ;
	Transform thisTransform;
	Transform camTransform;

	Texture2D tex;
	int life;
	int bars_seperator;
	int bars_size;
	public int nbars;

	
	void Start () 
	{	

	}

	public void setPropreties(){
		offset= new Vector3(-0.3f, 0.4f, 0.0f);
		bars_seperator = 1;
		bars_size = 10;
		life = nbars * (bars_size + bars_seperator) - bars_seperator;
		
		thisTransform = transform;
		if (useMainCamera)
			cam = Camera.main;
		else
			cam = cameraToUse;
		camTransform = cam.transform;
		
		tex = new Texture2D(life, 1);
		
		int i, j, count=0;
		for ( i =0; i<nbars; i++) {
			for( j=0; j<bars_size; j++){
				tex.SetPixel (count, 0, Color.green);
				count++;
			}
			
			if(i< nbars-1)
			for(j=0; j<bars_seperator; j++){
				tex.SetPixel (count, 0, Color.black);
				count++;
			}
		}
		
		tex.Apply ();
		
		guitex.texture = tex;
		guitex.pixelInset = new Rect (0, 0, 100, 20);
	}

	public void ReduceLife(int n_blocks){
		for (int n=0; n<n_blocks; n++) {
			if (life > 0) {
				for(int i=0; i<bars_size; i++){
					life--;
					tex.SetPixel(life, 0, Color.black);
				}
				if(life>0)
					for(int i=0; i<bars_seperator; i++)
						life--;
			}
			tex.Apply();
		}

	}
	
	void Update()
	{

		if (clampToScreen)
		{
			Vector3 relativePosition = camTransform.InverseTransformPoint(target.position);
			relativePosition.z =  Mathf.Max(relativePosition.z, 1.0f);
			thisTransform.position = cam.WorldToViewportPoint(camTransform.TransformPoint(relativePosition + offset));
			thisTransform.position = new Vector3(Mathf.Clamp(thisTransform.position.x, clampBorderSize, 1.0f - clampBorderSize),
			                                     Mathf.Clamp(thisTransform.position.y, clampBorderSize, 1.0f - clampBorderSize),
			                                     thisTransform.position.z);
			
		}
		else
		{
			thisTransform.position = cam.WorldToViewportPoint(target.position + offset);
		}


	}

}